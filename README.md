# php-fpm-web-server

## Getting started

```sh
# Import offline box file (https://app.vagrantup.com/ubuntu/boxes/bionic64)
$ vagrant box add ubuntu-bionic bionic-server-cloudimg-amd64-vagrant.box

# Run the machine
$ vagrant up

# Connect via SSH with 'vagrant' user (for developing purpose)
$ ssh vagrant@example.com -p 2222 -i /home/cto/Documents/Camp/Ansible/php-fpm-web-server/vagrant/example.com/.vagrant/machines/example.com/virtualbox/private_key

# Play Ansible roles
$ ansible-playbook playbook.yml
```
